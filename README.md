# README #

### What is this repository for? ###

* This a web application aims to help family law enquiry/case management system
* Version 1.0.0

### What tool do I need? ###

* Please download [Visual Studio Community](https://visualstudio.microsoft.com/vs/community/) 
* Please download [Docker Desktop](https://www.docker.com/products/docker-desktop/) 
* Please download [Dbeaver](https://dbeaver.io/)


### Configure Database ###
The application database can be configured by directly installing sql server to developer's machine or docker can be used for pulling 
latest sql server image from the server(this will prevent developer to install to pc manually). 

### Configure Database in Docker ###
Run the command after installing ``docker desktop``

```bash
	docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=yourpassword' -p 1433:1433 -d mcr.microsoft.com/mssql/server:2017-CU8-ubuntu
```

#### Database Initialisation ####
After the Docker images are running successfully :

Open Visual Studio and naviagte to ``Tools>NuGet Package Manager>Package Manager Console`` 

Run the command below :

```bash
	Update-Database
```

This will create tables in ``FamilyLawHub`` database.



