using FamilyLaw.Hub.Web.Infrastructure.Persistence.Context;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Model;
using FamilyLaw.Hub.Web.Infrastructure.Settings;
using FamilyLaw.Hub.Web.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FamilyLaw.Hub.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddDbContext<FamilyLawDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("FamilyLawHubDb")));

            services.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<FamilyLawDbContext>()
                .AddUserManager<UserService>()
                .AddDefaultTokenProviders();


            services.ConfigureApplicationCookie(options =>
            {
                options.AccessDeniedPath = new PathString("/Login");
                options.LoginPath = new PathString("/Login");
                options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
            });


            services.AddMvc();
            services.AddRazorPages()
                .AddRazorRuntimeCompilation();


            services.AddAuthorization(options =>
            {
                options.AddPolicy("Users-Create", policy => policy.RequireClaim("Users-Create"));
                options.AddPolicy("Users-View", policy => policy.RequireClaim("Users-View"));
                options.AddPolicy("Users-Details", policy => policy.RequireClaim("Users-Details"));
                options.AddPolicy("Users-Edit", policy => policy.RequireClaim("Users-Edit"));
                options.AddPolicy("Users-Delete", policy => policy.RequireClaim("Users-Delete"));
                options.AddPolicy("Roles-Create", policy => policy.RequireClaim("Roles-Create"));
                options.AddPolicy("Roles-View", policy => policy.RequireClaim("Roles-View"));
                options.AddPolicy("Roles-Details", policy => policy.RequireClaim("Roles-Details"));
                options.AddPolicy("Roles-Edit", policy => policy.RequireClaim("Roles-Edit"));
                options.AddPolicy("Roles-Delete", policy => policy.RequireClaim("Roles-Delete"));
                options.AddPolicy("Enquiry-Personal-Data-View", policy => policy.RequireClaim("Enquiry-Personal-Data-View"));
                options.AddPolicy("Enquiry-View", policy => policy.RequireClaim("Enquiry-View"));
                options.AddPolicy("Enquiry-Edit", policy => policy.RequireClaim("Enquiry-Edit"));

            });

            services.AddRouting(options =>
            {
                options.LowercaseUrls = true;
            });
            services.Configure<SmtpConfig>(Configuration.GetSection("SmtpConfig"));

            services.AddTransient<IEmailSender, EmailSender>();
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, 
            IHostingEnvironment env, 
            UserManager<User> userManager, 
            RoleManager<Role> roleManager)
        { 

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });

            DbUserInitializer.AddDefaultData(userManager, roleManager);

        }
    }
}
