﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FamilyLaw.Hub.Web.Migrations
{
    public partial class add_documentname_to_notes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DocumentName",
                table: "EnquiryNotes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DocumentName",
                table: "EnquiryNotes");
        }
    }
}
