﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FamilyLaw.Hub.Web.Migrations
{
    public partial class add_enquiry_notes_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EnquiryNotes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CaseEnquiryId = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnquiryNotes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EnquiryNotes_Enquiries_CaseEnquiryId",
                        column: x => x.CaseEnquiryId,
                        principalTable: "Enquiries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EnquiryNotes_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EnquiryNotes_CaseEnquiryId",
                table: "EnquiryNotes",
                column: "CaseEnquiryId");

            migrationBuilder.CreateIndex(
                name: "IX_EnquiryNotes_UserId",
                table: "EnquiryNotes",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EnquiryNotes");
        }
    }
}
