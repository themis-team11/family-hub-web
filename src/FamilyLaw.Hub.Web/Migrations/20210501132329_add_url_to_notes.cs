﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FamilyLaw.Hub.Web.Migrations
{
    public partial class add_url_to_notes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "EnquiryNotes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Url",
                table: "EnquiryNotes");
        }
    }
}
