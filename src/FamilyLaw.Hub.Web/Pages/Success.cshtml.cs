using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FamilyLaw.Hub.Web.Pages
{
    public class SuccessModel : PageModel
    {
        public IActionResult OnGet()
        {
            if (TempData.ContainsKey("SubmitSuccess") && 
                (bool)TempData["SubmitSuccess"])
            {
                return Page();
            }



            return RedirectToPage("Index");
        }
    }
}
