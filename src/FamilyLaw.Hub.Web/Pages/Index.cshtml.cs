﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using FamilyLaw.Hub.Web.Infrastructure;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Context;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Model;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Model.Enquiry;
using FamilyLaw.Hub.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace FamilyLaw.Hub.Web.Pages
{
    [BindProperties]
    public class IndexModel : PageModel
    {
        private readonly FamilyLawDbContext _dbContext;
        private readonly IEmailSender _emailSender;
        private readonly UserManager<User> _userManager;

        [Required(ErrorMessage = "Please enter your First Name", AllowEmptyStrings = false)]
        [StringLength(25)]
        public string FirstName { get; set; }


        [Required(ErrorMessage = "Please enter your Last Name", AllowEmptyStrings = false)]
        [StringLength(25)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter your email address")]
        [EmailAddress(ErrorMessage = "Please enter a valid email address")]
        public string EmailAddress { get; set; }


        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Please enter a valid phone number")]
        public string PhoneNumber { get; set; }

        public string Gender { get; set; }

        public List<SelectListItem> Genders { get; set; }

        public int AdviceType { get; set; }

        public List<SelectListItem> HowDidYouFoundUsOptions { get; set; }

        public string HowDidYouFoundUs { get; set; }

        public List<SelectListItem> AdviceTypes { get; set; }

        [Required(ErrorMessage = "Please enter your details about your familly matter", AllowEmptyStrings = false)]
        [StringLength(500)]
        public string FamilyMatter { get; set; }

        [StringLength(500)]
        public string ArgumentsWithPartner { get; set; }

        [StringLength(500)]
        public string ArgumentsWithFamily { get; set; }

        [StringLength(500)]
        public string InjuryFromFamily { get; set; }

        [StringLength(500)]
        public string HowSafeDoYouFeel { get; set; }

        [StringLength(500)]
        public string SpecialNeeds { get; set; }

        [StringLength(500)]
        public string SpecialNeedsChildren { get; set; }

        [StringLength(500)]
        public string RiskToChildren { get; set; }


        public IndexModel(FamilyLawDbContext dbContext, IEmailSender emailSender, UserManager<User> userManager)
        {
            _dbContext = dbContext;
            _emailSender = emailSender;
            _userManager = userManager;
        }

        public async Task<IActionResult> OnGet()
        {
            Genders = DropdownViewModel.GetGenders();
            HowDidYouFoundUsOptions = DropdownViewModel.GetMarketingOptions();

            AdviceTypes = await _dbContext.AdviceTypes.OrderBy(x => x.Name)
                .Select(x => new SelectListItem(x.Name,
                    x.Id.ToString()))
                .ToListAsync();

            return Page();
        }


        public async Task<IActionResult> OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var model = new CaseEnquiry
            {
                FirstName = FirstName,
                LastName = LastName,
                EmailAddress = EmailAddress,
                Phone = PhoneNumber,
                Gender = Gender,
                AdviceTypeId = AdviceType,
                FamilyMatter = FamilyMatter,
                ArgumentsWithPartner = ArgumentsWithPartner,
                ArgumentsWithFamily = ArgumentsWithFamily,
                HowSafeDoYouFeel = HowSafeDoYouFeel,
                SpecialNeeds = SpecialNeeds,
                SpecialNeedsChildren = SpecialNeedsChildren,
                RiskToChildren = RiskToChildren,
                HowDidYouFoundUs = HowDidYouFoundUs,
                Created = DateTime.UtcNow,
                Status = Status.AwaitingResponse
            };

            _dbContext.Enquiries.Add(model);

            await _dbContext.SaveChangesAsync();
            TempData["SubmitSuccess"] = true;

            await NotifyAdministrators(model.Id);

            return RedirectToPage("Success");
        }

        /// <summary>
        /// Notify Administrators
        /// </summary>
        /// <returns></returns>
        public async Task NotifyAdministrators(int enquiryId)
        {
            var adminUsers = await _userManager.GetUsersInRoleAsync(Roles.Administrator);

            var callbackUrl = Url.Page(
                "Admin/Enquiries/View",
                pageHandler: null,
                values: new { id = enquiryId },
                protocol: Request.Scheme);


            foreach (var user in adminUsers)
            {
                await _emailSender.SendEmailAsync(user.Email, "A new enquiry has been submitted!", 
                    $@"You have a new enquiry, please check <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>family law hub case management system</a>.");
            }
        }
    }
}
