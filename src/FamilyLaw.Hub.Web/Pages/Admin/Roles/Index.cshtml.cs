﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Context;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Model;
using FamilyLaw.Hub.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace FamilyLaw.Hub.Web.Pages.Admin.Roles
{
    [Authorize(Policy = "Roles-View")]
    public class IndexModel : PageModel
    {
        private readonly FamilyLawDbContext _context;

        public IndexModel(FamilyLawDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Role> Role { get;set; }

        public async Task OnGetAsync()
        {
            Role = await _context.Role.ToListAsync();
        }
    }
}
