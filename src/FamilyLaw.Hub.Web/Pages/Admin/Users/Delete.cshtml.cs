﻿using System.Threading.Tasks;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Context;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Model;
using FamilyLaw.Hub.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FamilyLaw.Hub.Web.Pages.Admin.Users
{
    [Authorize(Policy = "Users-Delete")]
    public class DeleteModel : PageModel
    {
        private readonly UserManager<User> _userManager;
        private readonly FamilyLawDbContext _context;

        public DeleteModel(UserManager<User> userManager, FamilyLawDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        [BindProperty]
        public int Id { get; set; }

        [BindProperty]
        public string UserName { get; set; }

        [BindProperty]
        public string FirstName { get; set; }

        [BindProperty]
        public string LastName { get; set; }


        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            // Get the user
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user.UserName == null)
            {
                return NotFound();
            }

            Id = user.Id;
            UserName = user.UserName;
            FirstName = user.FirstName;
            LastName = user.LastName;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user.UserName == null)
            {
                return NotFound();
            }

            var deleteUserResult = await _userManager.DeleteAsync(user);
            if (!deleteUserResult.Succeeded)
            {
                return Page();
            }

            return RedirectToPage("./Index");
        }
    }
}
