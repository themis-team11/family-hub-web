﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Context;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Model;
using FamilyLaw.Hub.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace FamilyLaw.Hub.Web.Pages.Admin.Users
{
    [Authorize(Policy = "Users-View")]
    public class IndexModel : PageModel
    {
        private readonly UserManager<User> _userManager;
        private readonly FamilyLawDbContext _dbContext;

        public IndexModel(UserManager<User> userManager,
            FamilyLawDbContext dbContext)
        {
            _userManager = userManager;
            _dbContext = dbContext;
        }

        [BindProperty]
        public IList<UserIndexViewModel> IndexModelList { get; set; }

        public class UserIndexViewModel
        {
            public int Id { get; set; }

            public string Username { get; set; }

            public string Email { get; set; }

            [Display(Name = "First Name")]
            public string FirstName { get; set; }

            [Display(Name = "Last Name")]
            public string LastName { get; set; }

            public string Roles { get; set; }
        }

        public async Task OnGetAsync()
        {
            var users = await _dbContext.User.Include(x => x.UserRoles).ToListAsync();
            var roles = await _dbContext.Roles.ToListAsync();
           
            IndexModelList = new List<UserIndexViewModel>();

            foreach (var user in users)
            {
                var role = string.Join(",",
                    roles.Where(x => user.UserRoles.Any(y => y.RoleId == x.Id)).Select(x => x.Name).ToList());

                IndexModelList.Add(new UserIndexViewModel
                {
                    Id = user.Id,
                    Username = user.UserName,
                    Email = user.Email,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Roles = role
                });
            }
        }
    }
}
