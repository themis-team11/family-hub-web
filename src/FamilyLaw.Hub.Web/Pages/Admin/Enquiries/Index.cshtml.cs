
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Context;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Model.Enquiry;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace FamilyLaw.Hub.Web.Pages.Admin.Enquiries
{
    [Authorize(Policy = "Enquiry-View")]
    public class IndexModel : PageModel
    {
        private readonly FamilyLawDbContext _dbContext;

        public IndexModel(FamilyLawDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<CaseEnquiry> Enquiries { get; set; }

        public async  Task<IActionResult> OnGet()
        {
            var result  = await _dbContext.Enquiries
                .Include(x => x.AdviceType)
                .Include(x => x.User)
                .ToListAsync();

            Enquiries = result.OrderByDescending(x => x.Id).ToList();

            return Page();
        }
    }
}
