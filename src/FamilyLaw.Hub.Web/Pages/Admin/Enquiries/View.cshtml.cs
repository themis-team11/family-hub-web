using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Context;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Model;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Model.Enquiry;
using FamilyLaw.Hub.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace FamilyLaw.Hub.Web.Pages.Admin.Enquiries
{
    [Authorize(Policy = "Enquiry-View")]
    [BindProperties]
    public class ViewModel : PageModel
    {

        private readonly FamilyLawDbContext _dbContext;
        private readonly UserManager<User> _userManager;

        public ViewModel(FamilyLawDbContext dbContext, UserManager<User> userManager)
        {
            _dbContext = dbContext;
            _userManager = userManager;
        }

        public List<NoteViewModel> Notes { get; set; }

        [Required(ErrorMessage = "Please enter your First Name", AllowEmptyStrings = false)]
        [StringLength(25)]
        public string FirstName { get; set; }


        [Required(ErrorMessage = "Please enter your Last Name", AllowEmptyStrings = false)]
        [StringLength(25)]
        public string LastName { get; set; }



        [Required(ErrorMessage = "Please enter your email address")]
        [EmailAddress(ErrorMessage = "Please enter a valid email address")]
        public string EmailAddress { get; set; }


        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Please enter a valid phone number")]
        public string PhoneNumber { get; set; }

        public string Gender { get; set; }

        public List<SelectListItem> Genders { get; set; }

        public int? Owner { get; set; }

        public string OwnerName { get; set; }

        public List<SelectListItem> Owners { get; set; }

        public int AdviceType { get; set; }

        public List<SelectListItem> HowDidYouFoundUsOptions { get; set; }

        public string HowDidYouFoundUs { get; set; }

        public List<SelectListItem> Statuses { get; set; }

        public string Status { get; set; }

        public List<SelectListItem> AdviceTypes { get; set; }

        [Required(ErrorMessage = "Please enter your details about your family matter", AllowEmptyStrings = false)]
        [StringLength(500)]
        public string FamilyMatter { get; set; }

        [StringLength(500)]
        public string ArgumentsWithPartner { get; set; }

        [StringLength(500)]
        public string ArgumentsWithFamily { get; set; }

        [StringLength(500)]
        public string InjuryFromFamily { get; set; }

        [StringLength(500)]
        public string HowSafeDoYouFeel { get; set; }

        [StringLength(500)]
        public string SpecialNeeds { get; set; }

        [StringLength(500)]
        public string SpecialNeedsChildren { get; set; }

        [StringLength(500)]
        public string RiskToChildren { get; set; }

        public async Task<IActionResult> OnGet(int id)
        {

            var enquiry = await _dbContext.Enquiries
                .Include(x => x.AdviceType)
                .Include(x => x.Notes)
                .ThenInclude(y => y.User)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (enquiry is null)
            {
                return NotFound();
            }

            FirstName = enquiry.FirstName;
            LastName = enquiry.LastName;
            EmailAddress = enquiry.EmailAddress;
            PhoneNumber = enquiry.Phone;
            Gender = enquiry.Gender;
            HowDidYouFoundUs = enquiry.HowDidYouFoundUs;
            AdviceType = enquiry.AdviceTypeId;
            FamilyMatter = enquiry.FamilyMatter;
            ArgumentsWithPartner = enquiry.ArgumentsWithPartner;
            ArgumentsWithFamily = enquiry.ArgumentsWithFamily;
            InjuryFromFamily = enquiry.InjuryFromFamily;
            HowSafeDoYouFeel = enquiry.HowSafeDoYouFeel;
            SpecialNeeds = enquiry.SpecialNeeds;
            SpecialNeedsChildren = enquiry.SpecialNeedsChildren;
            RiskToChildren = enquiry.RiskToChildren;
            Status = enquiry.Status.ToString();
            OwnerName = await _dbContext.User.Where(x => x.Id == enquiry.UserId).Select(x => $"{x.FirstName} {x.LastName}")
                .FirstOrDefaultAsync();
            Owner = enquiry.UserId;
            Genders = DropdownViewModel.GetGenders(Gender);
            HowDidYouFoundUsOptions = DropdownViewModel.GetMarketingOptions(HowDidYouFoundUs);
            Statuses = DropdownViewModel.GetStatuses(enquiry.Status);

            AdviceTypes = await _dbContext.AdviceTypes.OrderBy(x => x.Name)
                .Select(x => new SelectListItem(x.Name,
                    x.Id.ToString(), x.Id == AdviceType))
                .ToListAsync();
            Owners = AddDefaultOption(await _dbContext.User
                .Select(x => new SelectListItem($"{x.FirstName} {x.LastName}", x.Id.ToString(), x.Id == Owner))
                .ToListAsync(), "Please select an owner");


            Notes = enquiry.Notes
                .OrderByDescending(x => x.Created)
                .Select(x => new NoteViewModel(x))
                .ToList();


            return Page();
        }

        public async Task<IActionResult> OnPost(int id)
        {
            TempData["Notes"] = false;

            if (!ModelState.IsValid)
            {
                return Page();
            }

            var enquiry = await _dbContext.Enquiries
                .FirstOrDefaultAsync(x => x.Id == id);

            if (enquiry is null)
            {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(User);

            enquiry.FirstName = FirstName;
            enquiry.LastName = LastName;
            enquiry.EmailAddress = EmailAddress;
            enquiry.Phone = PhoneNumber;
            enquiry.Gender = Gender;
            enquiry.AdviceTypeId = AdviceType;
            enquiry.FamilyMatter = FamilyMatter;
            enquiry.ArgumentsWithPartner = ArgumentsWithPartner;
            enquiry.ArgumentsWithFamily = ArgumentsWithFamily;
            enquiry.HowSafeDoYouFeel = HowSafeDoYouFeel;
            enquiry.SpecialNeeds = SpecialNeeds;
            enquiry.SpecialNeedsChildren = SpecialNeedsChildren;
            enquiry.RiskToChildren = RiskToChildren;
            enquiry.HowDidYouFoundUs = HowDidYouFoundUs;
            enquiry.Updated = DateTime.UtcNow;
            enquiry.UserId = Owner;
            enquiry.Status = Enum.Parse<Status>(Status);
            enquiry.UpdatedBy = $"{user.FirstName} {user.LastName}";

            _dbContext.Enquiries.Update(enquiry);

            await _dbContext.SaveChangesAsync();

            return RedirectToPage("Index");
        }

        [StringLength(100)]
        public string NoteTitle { get; set; }

        [StringLength(5000)]
        public string Note { get; set; }

        public string DocumentUrl { get; set; }
        public string DocumentName { get; set; }
        public async Task<IActionResult> OnPostNote(int id)
        {
            TempData["Notes"] = true;
            TempData["NoteErrors"] = false;

            if (string.IsNullOrWhiteSpace(NoteTitle) || string.IsNullOrWhiteSpace(Note))
            {
                TempData["NoteErrors"] = true;
                return RedirectToPage("./View",id);

            }

            var user = await _userManager.GetUserAsync(User);

            await _dbContext.EnquiryNotes.AddAsync(new EnquiryNote
            {
                UserId = user.Id,
                CaseEnquiryId = id,
                Url = DocumentUrl,
                DocumentName = DocumentName,
                Title = NoteTitle,
                Note = Note,
                Created = DateTime.UtcNow
            });

            await _dbContext.SaveChangesAsync();

            return Redirect($"./{id}");
        }

        private List<SelectListItem> AddDefaultOption(List<SelectListItem> items, string defaultText)
        {
            if (items.Any(x => x.Selected))
            {
                return items;
            }

            var selectList = new List<SelectListItem>
            {
                new SelectListItem { Text = defaultText, Value = string.Empty, Disabled = true, Selected = true }
            };

            selectList.AddRange(items);

            return selectList;
        }

    }
}
