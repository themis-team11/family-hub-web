﻿namespace FamilyLaw.Hub.Web.Infrastructure.Settings
{
    public class SmtpConfig
    {
        public string From { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }

        public string Email { get; set; }
        public string Password { get; set; }
    }
}
