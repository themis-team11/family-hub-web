﻿namespace FamilyLaw.Hub.Web.Infrastructure
{
    public static class Roles
    {
        public const string Administrator = "Administrator";
        public const string Student = "Student";
    }
}
