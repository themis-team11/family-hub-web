﻿using System.Collections.Generic;

namespace FamilyLaw.Hub.Web.Infrastructure.Persistence.Model
{
    public static class UserClaims
    {
        private static List<string> _list;

        static UserClaims()
        {
            _list = new List<string>
            {
                "Users-View",
                "Users-Edit",
                "Users-Create",
                "Users-Delete",
                "Roles-View",
                "Roles-Edit",
                "Roles-Delete",
                "Roles-Create",
                "Enquiry-Edit",
                "Enquiry-View",
                "Enquiry-Personal-Data-View"
            };
        }

        public static List<string> GetList()
        {
            return _list;
        }
    }
}
