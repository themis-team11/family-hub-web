﻿using Microsoft.AspNetCore.Identity;

namespace FamilyLaw.Hub.Web.Infrastructure.Persistence.Model
{
    public class RoleClaim : IdentityRoleClaim<int>
    {
        public virtual Role Role { get; set; }
    }
}
