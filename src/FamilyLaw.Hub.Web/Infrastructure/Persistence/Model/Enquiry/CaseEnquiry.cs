﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FamilyLaw.Hub.Web.Infrastructure.Persistence.Model.Enquiry
{
    /// <summary>
    /// Enquiry
    /// </summary>
    public class CaseEnquiry : BaseEntity
    {
        public Status Status { get; set; }
        [StringLength(25)]
        public string FirstName { get; set; }
        [StringLength(25)]
        public string LastName { get; set; }

        [StringLength(200)]
        public string EmailAddress { get; set; }

        public string Phone { get; set; }

        public string Gender { get; set; }

        public AdviceType AdviceType { get; set; }

        [ForeignKey("AdviceType")]

        public int AdviceTypeId { get; set; }

        [StringLength(500)]
        public string FamilyMatter { get; set; }

        public DateTime Deadline { get; set; }

        public string DeadlinesDescription { get; set; }

        public string HowDidYouFoundUs { get; set; }

        [StringLength(500)]
        public string ArgumentsWithPartner { get; set; }

        [StringLength(500)]
        public string ArgumentsWithFamily { get; set; }

        [StringLength(500)]
        public string InjuryFromFamily { get; set; }

        [StringLength(500)]
        public string HowSafeDoYouFeel { get; set; }

        [StringLength(500)]
        public string SpecialNeeds { get; set; }

        [StringLength(500)]
        public string SpecialNeedsChildren { get; set; }

        [StringLength(500)]
        public string RiskToChildren { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Updated { get; set; }

        public string UpdatedBy { get; set; }

        public User User { get; set; }

        public int? UserId { get; set; }


        public List<EnquiryNote> Notes { get; set; }
    }
}
