﻿using System;


namespace FamilyLaw.Hub.Web.Infrastructure.Persistence.Model.Enquiry
{
    public class EnquiryNote : BaseEntity
    {
        public int CaseEnquiryId { get; set; }

        public CaseEnquiry CaseEnquiry { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public string Title { get; set; }

        public string Note { get; set; }

        public DateTime Created { get; set; }

        public string Url { get; set; }

        public string DocumentName { get; set; }
    }
}
