﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FamilyLaw.Hub.Web.Infrastructure.Persistence.Model.Enquiry
{
    
    public enum Status
    {
        AwaitingResponse,
        InProgress,
        Closed
    }
}
