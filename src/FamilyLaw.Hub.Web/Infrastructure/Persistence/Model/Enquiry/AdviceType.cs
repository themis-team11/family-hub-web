﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FamilyLaw.Hub.Web.Infrastructure.Persistence.Model.Enquiry
{
    public class AdviceType : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
