﻿namespace FamilyLaw.Hub.Web.Infrastructure.Persistence.Model.Enquiry
{
    public enum Priority
    {
        High,
        Medium,
        Low
    }
}
