﻿using Microsoft.AspNetCore.Identity;

namespace FamilyLaw.Hub.Web.Infrastructure.Persistence.Model
{
    public class UserLogin : IdentityUserLogin<int>
    {
        public virtual User User { get; set; }
    }
}
