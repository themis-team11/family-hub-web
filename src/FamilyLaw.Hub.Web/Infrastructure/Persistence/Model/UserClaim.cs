﻿using Microsoft.AspNetCore.Identity;

namespace FamilyLaw.Hub.Web.Infrastructure.Persistence.Model
{
    public class UserClaim : IdentityUserClaim<int>
    {
        public virtual User User { get; set; }
    }
}
