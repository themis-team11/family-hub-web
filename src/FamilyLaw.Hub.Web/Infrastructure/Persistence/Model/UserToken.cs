﻿using Microsoft.AspNetCore.Identity;

namespace FamilyLaw.Hub.Web.Infrastructure.Persistence.Model
{
    public class UserToken : IdentityUserToken<int>
    {
        public virtual User User { get; set; }
    }
}
