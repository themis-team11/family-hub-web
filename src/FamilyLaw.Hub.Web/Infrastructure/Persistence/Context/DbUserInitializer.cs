﻿using System.Linq;
using System.Security.Claims;
using FamilyLaw.Hub.Web.Infrastructure.Extensions;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Model;
using Microsoft.AspNetCore.Identity;

namespace FamilyLaw.Hub.Web.Infrastructure.Persistence.Context
{
    public static class DbUserInitializer
    {
        private static void AddRole(RoleManager<Role> roleManager, string roleName, string description)
        {
            var hasAdminRole = ConcurrencyHelper.RunSync(() => roleManager.RoleExistsAsync(roleName));

            if (hasAdminRole)
            {
                return;
            }

            ConcurrencyHelper.RunSync(() => roleManager.CreateAsync(new Role
            {
                Name = roleName,
                Description = description
            }));
        }


        private static void AddRoles(RoleManager<Role> roleManager)
        {
            AddRole(roleManager, Roles.Administrator, "System Administrator");
            AddRole(roleManager, Roles.Student, "Student with limited access");
        }

        private static void AddRoleClaims(RoleManager<Role> roleManager)
        {
            var role = ConcurrencyHelper.RunSync(() => roleManager.FindByNameAsync(Roles.Administrator));
            var roleClaims = ConcurrencyHelper.RunSync(() => roleManager.GetClaimsAsync(role));

            foreach (var claimString in UserClaims.GetList())
            {
                var newClaim = new Claim(claimString, "");
                if (roleClaims.All(rc => rc.Type.ToString() != claimString))
                {
                    ConcurrencyHelper.RunSync(() => roleManager.AddClaimAsync(role, newClaim));
                }
            }
        }

        private static void AddStudentClaims(RoleManager<Role> roleManager)
        {
            var role = ConcurrencyHelper.RunSync(() => roleManager.FindByNameAsync(Roles.Student));
            var roleClaims = ConcurrencyHelper.RunSync(() => roleManager.GetClaimsAsync(role));

            if (roleClaims.All(c => c.Type.ToString() != "Enquiry-View"))
            {
                ConcurrencyHelper.RunSync(() => roleManager.AddClaimAsync(role, new Claim("Enquiry-View", "Enquiry-View")));
            }
        }

        private static void AddUsers(UserManager<User> userManager)
        {
            var user = new User
            {
                UserName = "akuln@lsbu.ac.uk",
                Email = "akuln@lsbu.ac.uk",
                FirstName = "Nazim",
                LastName = "Akul",
                Enabled = true,
                EmailConfirmed = true
            };
            var result = ConcurrencyHelper.RunSync(() => userManager.CreateAsync(user, "F@zL1wb3"));

            if (result.Succeeded)
            {
                ConcurrencyHelper.RunSync(() => userManager.AddToRoleAsync(user, Roles.Administrator));
            }
        }

        public static void AddDefaultData(UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            AddRoles(roleManager);
            AddRoleClaims(roleManager);
            AddStudentClaims(roleManager);
            AddUsers(userManager);
        }
    }
}
