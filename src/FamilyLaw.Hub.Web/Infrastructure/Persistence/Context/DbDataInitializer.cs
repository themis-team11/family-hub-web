﻿using System.Collections.Generic;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Model.Enquiry;
using Microsoft.EntityFrameworkCore;

namespace FamilyLaw.Hub.Web.Infrastructure.Persistence.Context
{
    public static class DbDataInitializer
    {
        public static void Seed(ModelBuilder builder)
        {
            builder.Entity<AdviceType>()
                .HasData(new List<AdviceType>
                {
                    new AdviceType {Id = 1, Name = "Domestic abuse"},
                    new AdviceType {Id = 2, Name = "Divorce or dissolution partnership"},
                    new AdviceType {Id = 3, Name = "Unmarried separation"},
                    new AdviceType {Id = 4, Name = "Children"},
                    new AdviceType {Id = 5, Name = "Financial agreements/settlements "},
                    new AdviceType {Id = 6, Name = "Child abuse and neglect"},
                    new AdviceType {Id = 7, Name = "Other"},
                });
        }
    }
}