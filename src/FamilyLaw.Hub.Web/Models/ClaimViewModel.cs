﻿namespace FamilyLaw.Hub.Web.Models
{
    public class ClaimModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
