﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Model.Enquiry;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FamilyLaw.Hub.Web.Models
{
    public static class DropdownViewModel
    {

        private static Dictionary<string, Status> EnquiryStatus => new Dictionary<string, Status> {
            {"In Progress", Status.InProgress}, 
            {"Awaiting Response", Status.AwaitingResponse},
            {"Closed",Status.Closed}};
        private static string[] Genders => new[] { "Female", "Male", "Prefer not say", };
        private static string[] HowDidYouFoundUs => new[] { "Online search", "Word of mouth", "Lawyer", "Court", "Other advice organisation",
            "I have used this service before" };



        public static List<SelectListItem> GetStatuses(Status? status = null)
        {
            return EnquiryStatus.Select(x => new SelectListItem(x.Key, x.Value.ToString(),
                status.HasValue && status == x.Value)).ToList();
        }

        public static List<SelectListItem> GetGenders(string gender = null)
        {
            return Genders.Select(x => new SelectListItem(x,x, x == gender)).ToList();
        }

        public static List<SelectListItem> GetMarketingOptions(string howDidYouFoundUs = null)
        {
            return HowDidYouFoundUs.Select(x => new SelectListItem(x, x, x == howDidYouFoundUs)).ToList();
        }

    }
}
