﻿using System;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Model.Enquiry;
using NodaTime;

namespace FamilyLaw.Hub.Web.Models
{
    public class NoteViewModel
    {

        public NoteViewModel(EnquiryNote note)
        {
            var ukTimeZone = DateTimeZoneProviders.Tzdb["Europe/London"];
            var date = Instant.FromDateTimeUtc(DateTime.SpecifyKind(note.Created,DateTimeKind.Utc))
                .InZone(ukTimeZone)
                .ToDateTimeUnspecified()
                .ToString("yyyy/MMM/dd HH:mm");


            Id = note.Id;
            Title = note.Title;
            Note = note.Note;
            Created = date;
            CreatedBy = $"{note?.User?.FirstName} {note?.User?.LastName}";
            Url = note.Url;
            DocumentName = string.IsNullOrWhiteSpace(note.DocumentName) ? "View Document" : note.DocumentName;
        }

        public int Id { get; set; }

        public string Title { get; set; }

        public string Note { get; set; }

        public string Created { get; set; }

        public string CreatedBy { get; set; }

        public string Url { get; set; }
        public string DocumentName { get; set; }
    }
}
