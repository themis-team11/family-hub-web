﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FamilyLaw.Hub.Web.Infrastructure.Persistence.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace FamilyLaw.Hub.Web.Services
{

    public class UserService : UserManager<User>
    {
        public UserService(
                IUserStore<User> store, 
                IOptions<IdentityOptions> optionsAccessor, 
                IPasswordHasher<User> passwordHasher, 
                IEnumerable<IUserValidator<User>> userValidators, 
                IEnumerable<IPasswordValidator<User>> passwordValidators,
                ILookupNormalizer keyNormalizer,
                IdentityErrorDescriber errors,
                IServiceProvider services,
                ILogger<UserManager<User>> logger
            ) : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {    
        }

        public async Task<IdentityResult> ChangePasswordAsync(User user, string newPassword)
        {
            ThrowIfDisposed();

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (newPassword == null)
                throw new ArgumentNullException(nameof(newPassword));

            return await UpdatePasswordHash(user, newPassword, true);

        } 
    }
}
