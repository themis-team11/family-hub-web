﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using FamilyLaw.Hub.Web.Infrastructure.Settings;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;

namespace FamilyLaw.Hub.Web.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly SmtpConfig _config;

        public EmailSender(IOptions<SmtpConfig> smtpConfig)
        {
            _config = smtpConfig.Value;
        }

        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            var smtpClient = new SmtpClient(_config.Host)
            {
                Port = Convert.ToInt32(_config.Port),
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = true,
                Credentials = new NetworkCredential(_config.Email, _config.Password),
                EnableSsl = true
            };

            var mail = new MailMessage
            {
                From = new MailAddress(_config.From, "Family Law Hub @ LSBU"),
                Subject = subject,
                Body = htmlMessage,
                IsBodyHtml = true,
                To = {new MailAddress(email)}
            };

            smtpClient.Send(mail);

            return Task.CompletedTask;
        }
    }
}
